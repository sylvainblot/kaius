(function () { 
 return angular.module("appConstants", [])
.constant("Constants", {
  "CONFIG": {
    "CONNECTION_URL": "http://devbuild.kaios.com",
    "VERSION": "1.0.0"
  }
});

})();
