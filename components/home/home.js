angular.module('home', ['restAPIManager'])
    .component('home', {
        templateUrl: "components/home/home.html",
        controller: ['$scope', '$http', '$location', '$rootScope', 'restService', function($scope, $http, $location, $rootScope, restService) {
            var self = this;
            $scope.listViewOptions = {};
            $scope.search = {};
            $scope.categories = [];

            self.routeToCategory = function(category) {
                    $location.path('/category/'+category.name);
            };
            var softleftPressed = function() {
                self.get_categories();
            };
            var softrightPressed = function() {
                toastr.success("Made with love by the bananahackers <3")
            };

            self.get_categories = function() {
                console.log("Get categories");
                $http({
                    method: 'GET',
                    url: "http://51.158.70.138/store/categories.json",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    transformResponse: [
                       function (data) {
                           return JSON.parse(data);
                       }
                    ],
                }).then(function (response) {
                    naviBoard.setNavigation("home");
                    toastr.success("Categories downloaded.");
                      localStorage.setItem('categories', response.data);
                      $scope.categories = response.data;
                      console.error(response.data);
               });
            };

            self.$onInit = function() {

                self.get_categories();

                $rootScope.$emit("componentActive","home");
                $rootScope.$emit('changeHeader', "KaiUS");

                // restService.appInitiated("1.0.0")
                var footer = {
                    left: "",
                    right: "",
                    center: null
                };
                $rootScope.$emit('changeFooter', footer);
                naviBoard.refreshNavigation("home");


            }

            self.$onDestroy = function() {
                naviBoard.destroyNavigation("home");
            }
			
			var handleleft = $rootScope.$on('home_softleft', softleftPressed);
			$scope.$on("$destroy", handleleft);

			var handleright = $rootScope.$on('home_softright', softrightPressed);
			$scope.$on("$destroy", handleright);

        }]
    })
    .component('category', {
        templateUrl: "components/category/category.html",
        bindings: {
            categories: '<'
          },
        controller: ['$scope', '$location', '$http', '$rootScope', '$routeParams', function($scope, $location, $http, $rootScope, $routeParams) {
            var self = this;
			var ADDON_FILENAME = 'tmp_kaius.zip'

            $scope.listViewOptions = {};
            $scope.search = {};

            $scope.applications = [];

            var softleftPressed = function() {
                self.get_categories();
            };
            var softrightPressed = function() {
                self.backButtonPressed();
            };
			
            self.backButtonPressed = function() {
                $scope.$apply(function() {
                    $location.path('/home');
                })
            };
			
			// Credit : github @yzen https://gist.github.com/yzen/ce55479d7518743f11f6
            self.install = function(blob) {
				var sdcard = navigator.getDeviceStorage('sdcard');
				      // Delete the tempfile from the SD card.
				      var deleteRequest = sdcard.delete(ADDON_FILENAME);

				      deleteRequest.onsuccess = deleteRequest.onerror = function() {
						  toastr.success("Deleted.");
				        // Add the addon blob to the SD card using the temp filename.
				        var addNamedRequest = sdcard.addNamed(blob, ADDON_FILENAME);

				        addNamedRequest.onsuccess = function() {
				          // Retrieve the new tempfile.
				          var getRequest = sdcard.get(ADDON_FILENAME);

				          getRequest.onsuccess = function () {
				            var addonFile = getRequest.result;
							toastr.success("Got file.");
				            // Import the addon using the tempfile.
				            navigator.mozApps.mgmt.import(addonFile).then(function (addon) {
				              // Enable the addon by default.
				              toastr.success("Installed.");
				            }).catch(function(){
				            	toastr.success("Install error.");
				            });
				          };
				          getRequest.onerror = function (error) { toastr.success("Error installing: " + error); };
				        };
				        addNamedRequest.onerror = function(error) { toastr.success("Error add file on sdcard  storage: " + error); };
				      };
		      };
				
            self.installApp = function(app) {
                $('#loader').show();

                $http({
                    method: 'GET',
                    url: "http://51.158.70.138/store/files/"+$routeParams.category+"/"+app.name,
					responseType:'arraybuffer'
                }).then(function (response) {
                    $('#loader').hide();
                    toastr.success("Downloaded.");
					var blob = new Blob([response.data], {type: "octet/stream"});
					self.install(blob);
                }, function (response) {
                    $('#loader').hide();
                    toastr.error("Error download failed.");
                });

            };

            self.applicationInstall = function(){
                var activeElement = naviBoard.getActiveElement();
                self.installApp(app);
            };

            self.category_in_localstorage = function(category) {
                categories_saved = localStorage.getItem('categories_'+category);
                return (categories_saved !== null && JSON.parse(categories_saved)[category] !== null)
            };

            self.get_applications = function(category){
                applications_saved = localStorage.getItem('categories_'+category);

                //if (self.category_in_localstorage(category)) {
                //    $scope.applications = JSON.parse(applications_saved);
                //    $('#loader').hide();
                //    console.log($scope.applications);
                //} else {
                $http({
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: "http://51.158.70.138/store/category_"+category+".json",
                    transformResponse: [
                       function (data) {
                           return JSON.parse(data);
                       }
                   ],
                }).then(function (response) {
                    localStorage.setItem('categories_'+category, angular.toJson(response.data));
                    $scope.applications = response.data;
                    console.log("here");
                    $('#loader').hide();
                    naviBoard.setNavigation("category");
               })
           };

            self.$onInit = function() {
                $('#loader').show();
                $rootScope.$emit('changeHeader', $routeParams.category);
                $rootScope.$emit("componentActive", "category");
                self.get_applications($routeParams.category)

                var footer = {
                    left: "",
                    right: "",
                    center: null
                };
                $rootScope.$emit('changeFooter', footer);
                naviBoard.refreshNavigation("category");

            };

            self.$onDestroy = function() {
                naviBoard.destroyNavigation("category");
            };

			var handleleft = $rootScope.$on('home_softleft', softleftPressed);
			$scope.$on("$destroy", handleleft);

			var handleright = $rootScope.$on('home_softright', softrightPressed);
			$scope.$on("$destroy", handleright);
			
            var handle_back = $rootScope.$on('goBackFromCategory', self.backButtonPressed);
            $scope.$on("$destroy", handle_back);

        }]
    });